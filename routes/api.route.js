const router = require('express').Router();

const authenticate = require('./../middlewares/authenticate');
// load application routes
const authRoute = require('./../controllers/auth.route');
const userRoute = require('./../controllers/user.route');
const productRoute = require('./../modules/products/product.route');


router.use('/auth', authRoute);
router.use('/user',authenticate, userRoute);
router.use('/product', productRoute);

module.exports = router;
