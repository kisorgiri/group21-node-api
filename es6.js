// var vs let
// let variable declaration
// var==> functional scope
// let ==> block scope {},{}

// arrow notation function
// const welcome = function(name) {
//     // functional this
// }
// bikes.filter(function(item) {
//     if (item.brand === 'honda') {
//         return item;
//     }
// })
// bikes.filter(item => item.brand==='honda')
// const welcome = name => {
//     // arrow notation function will inherit this form parent
// }

// object 
// destruct
// short hand

// var a = 'apple';
// var b = {a};
// spread & rest

// import and export
// export way ==> 
// 1.named export
// export const fruits = ['apple'];
// export const vegitables = ['potato'];
// there can be multiple named export
// 2 default export
// export default fruits
// we can export default value only once
// importing from files
// if named export
// import {fruits,vegitables} from 'source file'
// if default export
// import xyz(own name)  from 'source file';

// import xyz,{fruits}  from 'source file';

// template literal
// 'hi '+name +' welcome to brodway'
// `hi ${name}, welcome to brodway`

