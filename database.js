// database is a container where we store and manipulate data
    // our database is mongodb which is document based database(distributed DBMS)
// shell command
// once mongodb is installed we can access at any location
// mongo this is a command to establish connection with mongodb server
// once connection established we will be given a > interface

// show dbs == list all available database
// use <db_name> if(existing db ) reselect it else create new db and select it
// db ==> shows selected db
//show collections ==> list all collection of selected db

// creating a collection
// db.<col_name>.insert({valid json})

//fetch a document
// db.<col_name>.find({query_builder});
// db.<col_name>.find({query_builder}).pretty()// format result

// update
// db.<col_name>.update({},{},{})
// 1st object is query builder
// 2nd object is payload it must contain $set as key
// 3rd options {multi:true,}

// remove
// db.<col_name>.remove({query_builder}) // dont keep empty object

// drop collection
// db.<col_name>.drop();

//drop database
// db.dropDatabase();

//#########db backup and restore #########
// bson | human redable(json csv)
// 1 bson
// backup
// mongodump ==> creates default dump folder and backup all the available database
// mongodump --db <db_name> dump selected database in dump folder
// mongodump --db <db_name> --out <path_to_destination_folder>

// restore
// mongorestore ==> checks for default dump folder and restore available database
// mongorestore <path_to_source_containing_backup>

// CSV and JSON
// json
// backup
// mongoexport  
// mongoexport --db <db_name> --collection <col_name> --out <path_to_destination_wit_.json_extension>
// mongoexport -d <db_name> -c <col_name> -o <path_to_destination_wit_.json_extension>

// import
// mongoimport
// mongoimport --db <db_name> --collection <col_name> source to json file

// csv
// mongoexport --db<db_name> --collection <col_name> --type=csv --fields 'comma separated field name' --out <path to destination> with csv extension

// import 
// mongoimport  --db <db_name> --collection <col_name> --type=csv path to source --headerline
//#########db backup and restore #########

// mongodump
// mongorestore

// mongoexport
// mongoimport