const ProductCtrl = require('./product.controller');

const router = require('express').Router();
const upload = require('./../../middlewares/uploader');
const isAdmin = require('./../../middlewares/uploader');
const authenticate = require('./../../middlewares/authenticate');

router.route('/')
    .get(authenticate, ProductCtrl.get)
    .post(authenticate, upload.single('img'), ProductCtrl.post);

router.route('/search')
    .get(ProductCtrl.search)
    .post(ProductCtrl.search)

router.route('/:id')
    .get(authenticate, ProductCtrl.getById)
    .put(authenticate, upload.single('img'), ProductCtrl.put)
    .delete(authenticate, ProductCtrl.remove);


module.exports = router;