var fs = require('fs');
function write(fileName, content, cb) {
    fs.writeFile('./files/' + fileName, content, function (err, done) {
        if (err) {
            cb(err);
        } else {
            cb(null, done);
        }
    })
}

function read(fileName) {
    return new Promise(function (resolve, reject) {
        fs.readFile('./files/' + fileName, 'UTF-8', function (err, done) {
            if (err) {
                reject(err);
            } else {
                console.log('data in read is >>', done);
                resolve(done)
            }
        })
    })

}


module.exports = {
    write,
    read
}