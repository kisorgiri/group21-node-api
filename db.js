const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');
mongoose.connect(`${dbConfig.conxnURL}/${dbConfig.dbName}`, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}, function(err, done) {
    if (err) {
        console.log('error connecting to db ', err);
    } else {
        console.log('db connection success')
    }
});

// template literal
// ` where we can type multiline string
// sldkjf
// sadfklsd
// sfdjlf
// sdf`
