const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: String,
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    phoneNumber: Number,
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    address: {
        permanent_address: String,
        temp_address: [String]
    },
    dob: Date,
    status: {
        type: Boolean,
        default: true
    },
    role: {
        type: Number, // 1 admin, 2 end-user 3 visitors
        default: 2
    },
    image: String
}, {
    timestamps: true
})

const UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;