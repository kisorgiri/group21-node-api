const express = require('express');
const app = express(); // app is entire express framework
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');
const config = require('./configs');
require('./db');


// view engine setup
var pug = require('pug');
app.set('view engine', pug);
app.set('views', path.join(__dirname, 'views'));

// third party middleware
app.use(morgan('dev'));
// allow every request
app.use(cors());

//import routing level middleware
const API_ROUTE = require('./routes/api.route');

// inbuilt middleware
app.use(express.static('files')); // internal serve within express
app.use("/file", express.static(path.join(__dirname, 'uploads')));
// parse incoming data
// xml
// json
// form-data // for files
// x-www-formurlencoded
app.use(express.urlencoded({
    extended: true
}))
app.use(express.json());

// load routing level middleware
app.use('/api', API_ROUTE);

// 404 error handler
app.use(function(req, res, next) {
        next({
            msg: 'Not Found',
            status: 404
        })
    })
    // error handling middleware
    // eror handling middleware is executed when next with argument is called
app.use(function(error, req, res, next) {
    res
        .status(error.status || 400)
        .json({
            msg: error.msg || error,
            status: error.status || 400
        })
});

app.listen(config.port, function(err, done) {
    if (err) {
        console.log('err in listening ');
    } else {
        console.log('server listening at port ' + config.port);
    }
})

// middleware
// middleware is a function that has access  to
// http request object
// http resposne object
// and next middleware function refrence

// syntax
// function (req,res,next){

// }

// middleware always came into action between request response cycle
// the order of middleware  is very important
// app.use() ia configuration block to use middleware
// any where app.use is used it is confiugrational block for middleware

// types of middleware
// 1 application level middleware
// 2 routing level middleware
// 3 inbuilt middleware
// 4 third party middleware
// 5 error handling middleware