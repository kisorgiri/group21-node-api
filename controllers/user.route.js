const router = require('express').Router();
const UserModel = require('./../models/user.model');
const map_user_req = require('./../helpers/map_user_req');
const upload = require('./../middlewares/uploader');

router.route('/')
    .get(function(req, res, next) {
        var condition = {};
        UserModel
            .find(condition, {
                email: 0
            })
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(1)
            .exec(function(err, users) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(users);
            });
    });


router.route('/dashboard')
    .get(function(req, res, next) {
        res.send('from dashboard of user');

    })
    .post(function(req, res, next) {
        console.log("here at post ", req.body);
        res.send('from user daashbaord');
    })
    .put(function(req, res, next) {

    })
    .delete(function(req, res, next) {

    });

router.route('/:id')
    .get(function(req, res, next) {
        UserModel.findById(req.params.id)
            .exec(function(err, user) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(user);
            })
    })

.delete(function(req, res, next) {
        UserModel.findById(req.params.id)
            .then(function(user) {
                if (user) {
                    user.remove(function(err, removed) {
                        if (err) {
                            return next(err);
                        }
                        res.status(200).json(removed);
                    })
                } else {
                    next({
                        msg: "user not found",
                        status: 404
                    })
                }
            })
            .catch(function(err) {
                next(err);
            })
    })
    .put(upload.single('img'), function(req, res, next) {
        var id = req.params.id;
        var data = req.body;
        if (req.file) {
            data.image = req.file.filename;
        }
        UserModel.findById(id, function(err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'user not found',
                    status: 404
                });
            }
            var updatedUser = map_user_req(user, data);
            updatedUser.save(function(err, updated) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(updated)
            })

        })
    });

module.exports = router;