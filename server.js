const http = require('http');

var server = http.createServer(function (req, res) {
    console.log('client connected to server');
    res.end('hi from node server');
    // regardless of any method or url each time this callback is executed
})

server.listen(9090, function (err, done) {
    if (err) {
        console.log('error listening ', err);
    } else {
        console.log('server listening at port 9090');
        console.log('press CTRL + C to exit');
    }
})